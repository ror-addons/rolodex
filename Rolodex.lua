Rolodex = {}
local nameList = {
										guildList = {},
										friendList = {},
										altList = {},
								}
local lookup = { alt = nameList.altList, guild = nameList.guildList, friend = nameList.friendList }

local currentType = ""
local filtered
local currentPrefix
local hookClose
local hookOpen

local updateRegistry = {}

local function cloneTable(t1, t2)
	for i,v in pairs(t1) do
		t2[i] = v
	end
end




function Rolodex.Setup()
	CreateWindow("RolodexFrame", false)
	hookClose = MailWindow.OnClose
	
	MailWindow.OnClose = Rolodex.Hide
	RegisterEventHandler(SystemData.Events.INTERACT_MAILBOX_OPEN, "Rolodex.Show")
	WindowRegisterEventHandler("RolodexFrame", SystemData.Events.GUILD_MEMBER_UPDATE, "Rolodex.Reload")
	WindowRegisterEventHandler("RolodexFrame", SystemData.Events.SOCIAL_FRIENDS_UPDATED, "Rolodex.Reload")
	Rolodex.Reload()
	Rolodex.RegisterEditBox("MailWindowTabSend", MailWindowTabSendToEditBox, "MailWindowTabSendToEditBox")

	LabelSetText("RolodexFrameTitleBarText", L"Rolodex")

	for row = 1, 10 do
    local targetRowWindow = "RolodexFrameListRow"..row.."Background"

    local row_mod = math.mod(row, 2)
    local color = DataUtils.GetAlternatingRowColor( row_mod )

    WindowSetTintColor(targetRowWindow, color.r, color.g, color.b )
    WindowSetAlpha(targetRowWindow, color.a )
  end
	
	

end
function Rolodex.Show()
	if WindowGetShowing("MailWindow") then
		WindowSetShowing("RolodexFrame", true)
		Rolodex.UpdateList()
	end
end

function Rolodex.ShowTooltip()
end

function Rolodex.Close()
	WindowSetShowing("RolodexFrame", false)
end

function Rolodex.Hide(...)
	Rolodex.Close()
	return hookClose(...)
end

local function LoadNameInfo(info, nameList)
	if not info then return end

	for i,v in ipairs(info) do
		local breaker
		local name = v.name or v.Name
		name = WStringToString(name):sub(1,-3)
		for i,thisname in ipairs(nameList) do
			if name == thisname then
				breaker = true
				break
			end
		end

		if not breaker and name:len() > 0 then
			table.insert(nameList, name)
		end
	end

	table.sort(nameList)
end

function Rolodex.Reload()
		LoadNameInfo(GetGuildMemberData(), nameList.guildList)
		LoadNameInfo(GetFriendsList(), nameList.friendList)
		LoadNameInfo(GameData.Account.CharacterSlot, nameList.altList)
end

local function FilterNames(box)
	local length = box.object.Text:len()
	local input = WStringToString(box.object.Text):sub(1, length):lower()
	for i=#lookup[currentType], 1, -1 do
		if	lookup[currentType][i]:len() < length or
			lookup[currentType][i]:sub(1, length):lower() ~= input then
			table.remove(lookup[currentType], i)
		end
	end
end

local function FilterAllNames(box)
	for type,v in pairs(lookup) do
		currentType = type
		FilterNames(box)
	end
	currentType = ""
end

function Rolodex.CheckInput()
	local parsed
	for name, box in pairs(updateRegistry) do
		if WindowGetShowing(name) then
			Rolodex.ParseInput(box)
			parsed = true
		end
	end
	if not parsed then
		Rolodex.Reload()
		Rolodex.UpdateList()
	end
end

function Rolodex.ParseInput(box)
	local string = WStringToString(box.object.Text):upper()
	local length = string:len()

	Rolodex.Reload()

	if length >= 1 then
			FilterAllNames(box)
	end
	Rolodex.UpdateList()
end

function Rolodex.Dummy()
end


local function AddList(header, type, list, index)
	
	Rolodex.itemHolder[index] = {header = header, type = type }

	table.insert(Rolodex.displayList, index)
	if currentType == type or currentType == "" then
		for _, name in ipairs(list) do
				index = index + 1
				Rolodex.itemHolder[index] = {name = StringToWString(name)}
				table.insert(Rolodex.displayList, index)
		end
	end
	
	if type == "guild" and #list == 0 then
		index = index + 1
		Rolodex.itemHolder[index] = {msg = L"Waiting on server data..."}
		table.insert(Rolodex.displayList, index)
		Rolodex.Reload()
	end

	return index + 1
end

function Rolodex.UpdateList(listType)
	Rolodex.itemHolder = {}
	Rolodex.displayList = {}


	if listType then
		currentType = listType
	end
	local index = 1

	index = AddList(L"Alternate Characters", "alt", nameList.altList, index)
	index = AddList(L"Friends", "friend", nameList.friendList, index)
	if GuildWindow.IsPlayerInAGuild() then
		index = AddList(L"Guild Members", "guild", nameList.guildList, index)
	end

	ListBoxSetDisplayOrder("RolodexFrameList", Rolodex.displayList)
end

function Rolodex.SetName()
	local rowIndex = WindowGetId(SystemData.ActiveWindow.name)

	dataIndex = ListBoxGetDataIndex("RolodexFrameList", rowIndex)

	local reference = Rolodex.itemHolder[dataIndex]

	if reference.header then
		if currentType ~= reference.type then
			Rolodex.UpdateList(reference.type)
		else
			currentType = ""
			Rolodex.CheckInput()
		end
		return
	end

	for name, box in pairs(updateRegistry) do
		if WindowGetShowing(name) then
			TextEditBoxSetText(box.name, reference.name)
		end
	end
end

function Rolodex.RegisterEditBox( WindowName, EditBoxObject, EditBoxName)
	updateRegistry[WindowName] = { object = EditBoxObject, name = EditBoxName }
	WindowRegisterCoreEventHandler(EditBoxName, "OnKeyTab", "Rolodex.CheckInput")
	WindowRegisterCoreEventHandler(EditBoxName, "OnTextChanged", "Rolodex.CheckInput")
end

function Rolodex.UnregisterEditBox( WindowName )
	WindowUnregisterCoreEventHandler(updateRegistry[WindowName].name, "OnKeyTab")
	WindowUnregisterCoreEventHandler(updateRegistry[WindowName].name, "OnTextChanged")
	updateRegistry[WindowName] = nil
end
