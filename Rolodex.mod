<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Rolodex" version="1.4.4" date="02/10/2008">
		<Author name="Wobin" email="wobster@gmail.com"/>
		<Description text="Offers a dynamic list of names for the 'To' field when sending mail."/>
		<Files>
			<File name="Rolodex.xml"/>
		</Files>
		<Dependencies>
			<Dependency name="EA_MailWindow"/>
		</Dependencies>
		<OnInitialize>
			<CallFunction name="Rolodex.Setup"/>
		</OnInitialize>
		<OnShutdown/>
	</UiMod>
</ModuleFile>
